from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class SceleFunctionalTest(TestCase):
    def setUp(self):
        self.selenium = webdriver.Chrome()
        super(SceleFunctionalTest, self).setUp()
    def tearDown(self):
        self.selenium.quit()
        super(SceleFunctionalTest, self).tearDown()
    def test_title(self):
        selenium = self.selenium
        selenium.get("https://scele.cs.ui.ac.id/")
        time.sleep(2)
        searchbox = selenium.find_element_by_id("coursesearchbox")
        searchbox.send_keys("PPW")
        time.sleep(2)
        searchbox.send_keys(Keys.ENTER)
        time.sleep(2)
        self.assertIn("Search results: 6", selenium.find_element_by_css_selector("#region-main div h2").text)
        self.assertIn("[KI] Perancangan & Pemrograman Web", selenium.find_element_by_css_selector(".courses.course-search-result").text)
        time.sleep(2)
selenium = webdriver.Chrome()
selenium.maximize_window()
selenium.get("https://scele.cs.ui.ac.id/")
time.sleep(2)
searchbox = selenium.find_element_by_id("coursesearchbox")
searchbox.send_keys("PPW")
time.sleep(2)
searchbox.send_keys(Keys.ENTER)
time.sleep(2)
time.sleep(2)
selenium.quit()
selenium.assertIn("Search results: 6",selenium.find_element_by_css_selector("#region-main div h2").text)
selenium.assertIn("[KI] Perancangan & Pemrograman Web", selenium.find_element_by_css_selector(".courses.course-search-result").text)

selenium = webdriver.Chrome()
selenium.maximize_window()

selenium.get("http://localhost:8000/")

time.sleep(2)

message_box = selenium.find_element_by_id("id_message").click()
time.sleep(2)
message_box.send_keys("Hello.. Testing 1 2 3")
time.sleep(2)
message_box.send_keys(Keys.ENTER)
time.sleep(2)
selenium.quit()
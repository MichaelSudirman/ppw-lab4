// Navbar Materialize
$(document).ready(function () {
  $(".button-collapse").sideNav();
});

// Smooth Scrolling 
$(".scroller").click(function (e) {
  e.preventDefault();
  $('body,html').animate({
    scrollTop: $(this.hash).offset().top - 65
  }, 1000);
});

// Date Picker

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth(); //January is 0!
var yyyy = today.getFullYear();

$('.datepicker').pickadate({
  format: "yyyy-mm-dd",
  selectMonths: true, // Creates a dropdown to control month
  selectYears: 100, // Creates a dropdown of 15 years to control year
  autoclose: true,
  // selectMonths: true,
  // selectYears: 15,
  // required:true
  // min: new Date(1900,1,1),
  // max: new Date(yyyy,mm,dd)
});
function checkDate() {
  if ($('#datepick').val() == '') {
     $('#datepick').addClass('invalid')
     return false;
   } else {
     $('#datepick').removeClass('invalid')
     return true;
   }
 }
 $('#schedule-form').submit(function() {
   return checkDate();
 });
 $('#datepick').change(function() {
   checkDate();
 });

$('.timepicker').pickatime({
  format: "HH:mm p",
  default: 'now',
  twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
  // donetext: 'OK',
  // autoclose: false,
  // vibrate: true // vibrate the device when dragging clock hand
});
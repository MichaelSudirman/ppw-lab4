$(document).ready(function () {


    $('#status-theme').click(function () {
        if ($('#page-container').css('color').toLowerCase() == 'rgb(0, 0, 0)') {
            $('#page-container').css('color', 'red');
            $('body').css('background-color', 'lightgoldenrodyellow');
            $('theme').html('Theme: Red');
            $('h1').first().css('color', 'red');

            $('.btn-override').addClass('btn-outline-danger');
            $('.btn-override').removeClass('btn-outline-dark');
            $('.card-override').addClass('bg-danger');
            $('.card-override').removeClass('bg-dark');

        }
        else {
            $('#page-container').css('color', 'black');
            $('body').css('background-color', 'white');
            $('theme').html('Theme: Light');
            $('h1').first().css('color', 'black');

            $('.btn-override').removeClass('btn-outline-danger');
            $('.btn-override').addClass('btn-outline-dark');
            $('.card-override').removeClass('bg-danger');
            $('.card-override').addClass('bg-dark');
        }
    });


    $('#profile-theme').click(function () {
        if ($('#page-container').css('color').toLowerCase() == 'rgb(0, 0, 0)') {
            $('#page-container').css('color', 'red');
            $('body').css('background-color', 'lightgoldenrodyellow');
            $('theme').html('Theme: Red');
            $('h1').first().css('color', 'red');

            $('.btn-override').addClass('btn-outline-danger');
            $('.btn-override').removeClass('btn-outline-dark');
            $('.accordion-header').addClass('bg-danger');
            $('.accordion-content').addClass('bg-danger');
            $('.accordion-header').removeClass('bg-dark');
            $('.accordion-content').removeClass('bg-dark');

        }
        else {
            $('#page-container').css('color', 'black');
            $('body').css('background-color', 'white');
            $('theme').html('Theme: Light');
            $('h1').first().css('color', 'black');

            $('.btn-override').removeClass('btn-outline-danger');
            $('.btn-override').addClass('btn-outline-dark');
            $('.accordion-header').addClass('bg-dark');
            $('.accordion-content').addClass('bg-dark');
            $('.accordion-header').removeClass('bg-danger');
            $('.accordion-content').removeClass('bg-danger');
        }

    });

    $('.accordion-header').click(function () {
        $(this).next().toggleClass('accordion-content-hidden');
        $('.accordion-content').not($(this).next()).addClass('accordion-content-hidden');
    });

    setTimeout(function () {
        $('body').addClass('loaded', () => {
            $('#loader-wrapper').css({ "display": "none" });
        });
        $('h1').css('color', '#222222');
    }, 500);

    $('#organization-button').click(
        function () {
            $.ajax({
                url: "/static/data.json",
                success: function (result) {
                    for (var i = 0; i < result.members.length; i++) {
                        $('div#accordion-2.accordion-content').append("<tr style='padding:0px 50px'>" + "<th>" 
                        + result.members[i].name + "</th>" + "-" + "<th> - " 
                        + result.members[i].description + "</th>" + "</tr>")
                    }
                }
            });
        }
    );

});
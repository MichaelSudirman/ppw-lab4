$(document).ready(function () {
    function addToFavorites() {
        alert('test')
    }
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
        success: function (result) {
            console.log(result.items)
            for (var i = 0; i < result.items.length; i++) {
                // console.log(result.items[i])
                $('tbody').append('<tr>'
                    + '<th>' + i + '</th>'
                    + '<th><img src="' + result.items[i].volumeInfo.imageLinks.smallThumbnail + '"></th>'
                    + '<th class="title-column">' + result.items[i].volumeInfo.title + '</th>'
                    + '<th class="author-column">' + result.items[i].volumeInfo.authors + '</th>'
                    + '<th class="favorite-column">' + '<i id="favorite-star" class="material-icons">star</i>' + '</th>'
                    + '</tr>')
            }
        }
    });
    var favoriteCount = 0
    $(document).on("click", "#favorite-star", function () {
        console.log('test')
        if ($(event.target).hasClass('active-star')) {
            console.log('active')
            $(event.target).removeClass('active-star')
            favoriteCount--
        }
        else {
            console.log('not active')
            $(event.target).addClass('active-star')
            favoriteCount++
        }
        console.log(favoriteCount)
        $('.favorites-counter').html('<h1>' + favoriteCount + '</h1>    ')
    });

    $('#filter-button').click(function () {
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
            success: function (result) {
                favoriteCount = 0
                $('.favorites-counter').html('<h1>' + favoriteCount + '</h1>')
                $('tbody').html('')
                console.log('button-event')
                console.log(result.items)
                console.log($('#filter-textarea').val())
                var searchText = $('#filter-textarea').val()
                var counter = 0
                for (var i = 0; i < result.items.length; i++) {
                    console.log(result.items[i].volumeInfo.title.includes(searchText))
                    if (result.items[i].volumeInfo.title.includes(searchText)) {
                        $('tbody').append('<tr>'
                            + '<th>' + counter + '</th>'
                            + '<th><img src="' + result.items[i].volumeInfo.imageLinks.smallThumbnail + '"></th>'
                            + '<th class="title-column">' + result.items[i].volumeInfo.title + '</th>'
                            + '<th class="author-column">' + result.items[i].volumeInfo.authors + '</th>'
                            + '<th class="favorite-column">' + '<i id="favorite-star" class="material-icons">star</i>' + '</th>'
                            + '</tr>')
                        counter++
                    }
                }
            }

        });
    });
});
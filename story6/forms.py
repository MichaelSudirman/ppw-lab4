from django import forms

class add_status(forms.Form):
    message_attrs = {
        'type' : 'text',
        'class' : 'validate',
        'placeholder' : 'Message'
    }
    

    message = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=message_attrs))
    
from django.db import models

# Create your models here.

class Status(models.Model):
    id = models.AutoField(primary_key=True)
    message = models.CharField(max_length=100)
    
from django.urls import include,path
from django.contrib import admin
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('postMessage',views.postMessage, name='postMessage'),
    path('postProfile',views.postProfile, name ='postProfile'),
    path('postDelete/<int:idPost>/',views.postDelete, name='postDelete'),
]
from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect

# create schedule models
from .forms import add_status
from .models import Status

#testing
from django.views.decorators.csrf import requires_csrf_token

def index(request):
    all_status = Status.objects.all()
    form = add_status()

    response = {
        'status_form' : add_status,
        'all_status' : all_status,
        'form' : form,
    }

    return render(request, 'story6/index.html',response)

def postMessage(request):
    all_status = Status.objects.all()
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = add_status(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # return HttpResponseRedirect('/thanks/')
            cleaned_data = form.cleaned_data
            status = Status(message = cleaned_data['message'])
            status.save()
            return HttpResponseRedirect('postMessage')
        # return HttpResponse('error')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = add_status()

    response = {
        'status_form' : add_status,
        'all_status' : all_status,
        'form' : form,
    }

    return render(request, 'story6/index.html',response)

def postDelete(request,idPost):
    Status.objects.get(id=idPost).delete()
    return HttpResponseRedirect('../../postMessage')

def postProfile(request):
    return render(request,'story6/postProfile.html')

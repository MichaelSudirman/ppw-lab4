from django.test import TestCase,Client

# Create your tests here.
from django.urls import resolve
from .views import index
from .forms import add_status
from .models import Status
from django.utils import timezone

# Lab 7
# from django.test import TestCase
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# import time
# from selenium.webdriver.chrome.options import Options
# from django.contrib.staticfiles.testing import StaticLiveServerTestCase

class Lab4Test(TestCase):
    def test_url_post_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code,200)

    def test_url_not_exist(self):
        response = Client().get('doesnotexist')
        self.assertEqual(response.status_code,404)

    def test_lab_3_using_to_do_list_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_lab_3_using_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_activity(self):
        #Creating a new activity
        new_activity = Status.objects.create(message = "this is a text")
        #Retrieving all available activity
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)
    
    def test_postProfile(self):
        response = Client().get('/story6/postProfile')
        self.assertEqual(response.status_code,200)

    def test_form_valid_data(self):
        form_data = {'message': 'Hello'}
        form = add_status(data=form_data)
        self.assertTrue(form.is_valid())

    def test_remove_status(self):
        response = Client().post('/add-status/',{'text':'TestStatus'})
        added_amount = Status.objects.all().count()
        response = Client().get('/remove-status/1')
        removed_amount = Status.objects.all().count()
        self.assertEquals(removed_amount,0)
    
    


# class Lab7Test(StaticLiveServerTestCase):
#     def setUp(self):
#         self.options = Options()
#         self.options.add_argument('--dns-prefetch-disable')
#         self.options.add_argument('--no-sandbox')
#         self.options.add_argument('--headless')
#         self.options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome(chrome_options=self.options, executable_path='./chromedriver')
#         super(Lab7Test, self).setUp()
#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab7Test, self).tearDown()

#     def test_story6_have_header(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + '/story6')
#         header = selenium.find_elements_by_tag_name("h1")
#         self.assertIn("Hello, how are you?",header[0].text)
#         self.assertIn("1",str(len(header)))

#     def test_story6_have_subheader(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + '/story6')
#         subheader = selenium.find_elements_by_tag_name("p")
#         self.assertIn("Please share your text here",subheader[0].text)


#     def test_story6_button_color(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + '/story6')
#         delete_button = selenium.find_element_by_css_selector("a.btn.btn-outline-dark.btn-override.head-btn-override").value_of_css_property('color')
#         self.assertIn("rgba(52, 58, 64, 1)",delete_button)

#     def test_story6_text_area_font_size(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + '/story6')
#         text_area= selenium.find_element_by_css_selector("a.btn.btn-outline-dark.btn-override.head-btn-override").value_of_css_property('font-size')
#         self.assertIn("16px",text_area)

#     def test_story6_have_status(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + '/story6')
#         message_box = selenium.find_element_by_id("id_message")
#         message_box.click()
#         message_box.send_keys("Hello.. Testing 1 2 3")
#         message_box.send_keys(Keys.ENTER)
#         self.assertIn("Hello.. Testing 1 2 3", selenium.find_elements_by_css_selector("p.card-text")[-1].text)
#         delete_button = selenium.find_elements_by_id("delete_button")[-1]
#         delete_button.click()
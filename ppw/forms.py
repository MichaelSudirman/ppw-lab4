from django import forms

class add_schedule(forms.Form):
    name_attrs = {
        'type' : 'text',
        'class' : 'validate',
        'placeholder' : 'Name'
    }
    category_attrs = {
        'type' : 'text',
        'class' : 'validate',
        'placeholder' : 'Category'
    }
    date_attrs = {
        'id' : 'datepick',
        'type' : 'text',
        'class': 'datepicker',
        'placeholder' : 'Date'
    }
    time_attrs = {
        'type' : 'text',
        'class': 'timepicker',
        'placeholder' : 'Time'

    }
    location_attrs = {
        'type' : 'text',
        'class' : 'validate',
        'placeholder' : 'Location'
    }

    name = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=name_attrs))
    category = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=category_attrs))
    date = forms.DateField(label='', required=True, widget=forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label='', required=True, widget=forms.TimeInput(attrs=time_attrs))
    location = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=location_attrs))
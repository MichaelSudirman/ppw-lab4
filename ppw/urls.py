from django.urls import include,path
from django.contrib import admin
from . import views

urlpatterns = [
    path('',views.index, name='index'),
    path('signup',views.signup, name='signup'),
    path('background',views.background, name='background'),
    path('showSchedule',views.showSchedule, name='showSchedule'),
    path('addSchedule',views.addSchedule, name='addSchedule'),
    path('delSchedule',views.delSchedule, name='delSchedule')
]
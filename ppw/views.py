from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect

# create schedule models
from .forms import add_schedule
from .models import Schedule

#testing
from django.views.decorators.csrf import requires_csrf_token

# mhs_name = 'Michael'

def index(request):
    return render(request, 'ppw/index.html')
    # return HttpResponse ("Hello world!")
    # response = {'name': mhs_name}
    # return render(request, 'index.html', response)
    # return render(request, 'index.html')
    

def signup(request):
    return render(request, 'ppw/signup.html')

def background(request):
    return render(request, 'ppw/background.html')

def showSchedule(request):
    all_schedule = Schedule.objects.all()
    response = {
        'all_schedule' : all_schedule,
    }
    return render(request,'ppw/showSchedule.html', response)

def addSchedule(request):
    all_schedule = Schedule.objects.all()

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = add_schedule(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # return HttpResponseRedirect('/thanks/')
            cleaned_data = form.cleaned_data
            schedule = Schedule(name = cleaned_data['name'], category = cleaned_data['category'], date = cleaned_data['date'], time = cleaned_data['time'], location = cleaned_data['location'])
            schedule.save()
            return HttpResponseRedirect('showSchedule')
        return HttpResponse('error')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = add_schedule()

    response = {
        'schedule_form' : add_schedule,
        'all_schedule' : all_schedule,
        'form' : form 
    }

    # return render(request,'schedule.html', {'form' : form})
    
    return render(request,'ppw/addSchedule.html', response)

def delSchedule(request):
    Schedule.objects.all().delete()
    
    return render(request,'ppw/showSchedule.html')
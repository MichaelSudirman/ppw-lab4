from django.db import models

class Schedule(models.Model):
    name = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=30)

    def __str__(self):
        return self.name
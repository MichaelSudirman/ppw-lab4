$(document).ready(function () {
    var favoriteCount = 0
    if (sessionStorage.getItem('favoriteCounter') == null) {
        sessionStorage.setItem('favoriteCounter', 0);
    }
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
        success: function (result) {
            console.log(result.items)
            for (var i = 0; i < result.items.length; i++) {
                // console.log(result.items[i])
                $('tbody').append('<tr>'
                    + '<th>' + i + '</th>'
                    + '<th><img src="' + result.items[i].volumeInfo.imageLinks.smallThumbnail + '"></th>'
                    + '<th class="title-column">' + result.items[i].volumeInfo.title + '</th>'
                    + '<th class="author-column">' + result.items[i].volumeInfo.authors + '</th>'
                    + '<th class="favorite-column">' + '<i id="favorite-star" class="material-icons">star</i>' + '</th>'
                    + '</tr>')
            }

            $('.favorites-counter').html('<h1>' + sessionStorage.getItem('favoriteCounter') + '</h1>')
        }
    });
    $(document).on("click", "#favorite-star", function () {
        console.log('test')
        if ($(event.target).hasClass('active-star')) {
            console.log('active')
            $(event.target).removeClass('active-star')
            // favoriteCount--
            sessionStorage.favoriteCounter--
        }
        else {
            console.log('not active')
            $(event.target).addClass('active-star')
            // favoriteCount++
            sessionStorage.favoriteCounter++
        }
        console.log(favoriteCount)
        // $('.favorites-counter').html('<h1>' + favoriteCount + '</h1>')
        $('.favorites-counter').html('<h1>' + sessionStorage.getItem('favoriteCounter') + '</h1>')
    });

    $('#filter-button').click(function () {
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
            success: function (result) {
                // favoriteCount = 0
                // $('.favorites-counter').html('<h1>' + favoriteCount + '</h1>')
                $('.favorites-counter').html('<h1>' + sessionStorage.getItem('favoriteCounter') + '</h1>')
                $('tbody').html('')
                console.log('button-event')
                console.log(result.items)
                console.log($('#filter-textarea').val())
                var searchText = $('#filter-textarea').val()
                var counter = 0
                for (var i = 0; i < result.items.length; i++) {
                    console.log(result.items[i].volumeInfo.title.includes(searchText))
                    if (result.items[i].volumeInfo.title.includes(searchText)) {
                        $('tbody').append('<tr>'
                            + '<th>' + counter + '</th>'
                            + '<th><img src="' + result.items[i].volumeInfo.imageLinks.smallThumbnail + '"></th>'
                            + '<th class="title-column">' + result.items[i].volumeInfo.title + '</th>'
                            + '<th class="author-column">' + result.items[i].volumeInfo.authors + '</th>'
                            + '<th class="favorite-column">' + '<i id="favorite-star" class="material-icons">star</i>' + '</th>'
                            + '</tr>')
                        counter++
                    }
                }
            }

        });
    });
    $('#logout-button').click(function () {
        sessionStorage.removeItem('favoriteCounter');
    })
    $('#user_email').focusout(function () {
        const emailInput = $('#user_email').val()
        $.ajax({
            url: "./api/getEmail",
            success: function (result) {
                console.log(result)
                const data = result.data;
                let bool = false;
                data.forEach((email, id) => {
                    if (email.email === emailInput) {
                        bool = true;
                    }
                })
                valid = bool
                if (valid) {
                    $('#notif-error').text("The email has already been registered, please register with another email!")
                    $('#button-form-validate').prop('disabled', true)
                } else {
                    $('#notif-error').text("")
                    $('#button-form-validate').prop('disabled', false)
                    buttonQuery()
                }
            }
        })
    });

    function buttonQuery() {
        let name = $('#user_name').val();
        let password = $('#user_password').val();
        let email = $('#email').val();
        console.log(name.length);
        console.log(password.length);
        console.log(valid);
        // if(!valid && passworld)
    }
    $("form").on("submit", function (event) {
        event.preventDefault();
        var data = $(this).serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value
            return obj
        }, {});

        console.log(data);
        var csrf_token = data['csrfmiddlewaretoken'];
        var name = data['name'];
        var email = data['email'];
        var password = data['password'];

        $.ajax({
            url: "../story9/api/register",
            type: "POST",
            data: {
                csrfmiddlewaretoken: data.csrfmiddlewaretoken,
                name: data.name,
                email: data.email,
                password: data.password,
            },
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", data.csrfmiddlewaretoken)
                }
            },
            success: function (result) {
                if (result.success == true) {
                    alert("You have successfully registered")
                } else {
                    alert("Oops something went wrong")
                }
            }
        });
    });

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

});

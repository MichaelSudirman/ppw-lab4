from django.shortcuts import render,reverse

# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect
from .forms import add_subscriber
from .models import Subscriber

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate,logout

def index(request):
    return render(request, 'story9/index.html')

def register(request):
    # all_subscriber = Subscriber.objects.all()

    if request.method == 'POST':
        form = add_subscriber(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            subscriber = Subscriber(
                name = cleaned_data['name'],
                password = cleaned_data['password'],
                email = cleaned_data['email'])
            subscriber.save()
            return HttpResponseRedirect('register')
        
        return HttpResponse('error')
    else:
        form = add_subscriber()
    
    response = {
        'form' : form
    }
    return render(request, 'story9/register.html',response)

def get_email(request):
    subs = Subscriber.objects.all()
    data = {"data":list(subs.values("email"))}
    return JsonResponse(data)

def api_register(request): 
    if (request.method == 'POST'): 
        form = add_subscriber(request.POST) 
        if (form.is_valid()): 
            name = form.cleaned_data['name'] 
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']  
            new_sub = Subscriber(name=name, password=password, email=email) 
            new_sub.save() 
            return JsonResponse({'success': True}) 
    return JsonResponse({'success': False})

def logout_page(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('story9:index'))
    # response = HttpResponseRedirect('./index')
    # logout(request)
    # return response
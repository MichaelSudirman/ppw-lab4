from django import forms

class add_subscriber(forms.Form):
    name_attrs = {
        'id' : 'user_name',
        'type' : 'text',
        'class' : 'form-control form-override',
        'placeholder' : 'Name'
    }
    password_attrs = {
        'id' : 'user_password',
        'type' : 'password',
        'class' : 'form-control form-override',
        'placeholder' : 'Password'
    }
    email_attrs = {
        'id' : 'user_email',
        'type' : 'email',
        'class' : 'form-control form-override',
        'placeholder' : 'Email'
    }

    name = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    password = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=password_attrs))
    email = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=email_attrs))
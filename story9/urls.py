from django.urls import include,path
from django.contrib import admin
from . import views

app_name = 'story9'
urlpatterns = [
    path('',views.index,name='index'),
    path('register',views.register,name='register'),
    path('api/getEmail',views.get_email,name="get_email"),
    path('api/register',views.api_register,name='api_register'),
    path('logout/',views.logout_page,name='logout'),
]